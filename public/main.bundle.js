webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages__ = __webpack_require__("./src/app/pages/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot([
                    { path: '', redirectTo: '/home', pathMatch: 'full' },
                    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_2__pages__["a" /* IndexComponent */] },
                    { path: 'portfolio/:name', component: __WEBPACK_IMPORTED_MODULE_2__pages__["b" /* PortfolioComponent */] },
                ])
            ],
            declarations: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_routing_module__ = __webpack_require__("./src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components__ = __webpack_require__("./src/app/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages__ = __webpack_require__("./src/app/pages/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_4__components__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_4__components__["b" /* NavbarComponent */], __WEBPACK_IMPORTED_MODULE_4__components__["c" /* ProfileComponent */], __WEBPACK_IMPORTED_MODULE_4__components__["d" /* SectionComponent */], __WEBPACK_IMPORTED_MODULE_4__components__["e" /* SinglePackItemComponent */],
                __WEBPACK_IMPORTED_MODULE_5__pages__["a" /* IndexComponent */], __WEBPACK_IMPORTED_MODULE_5__pages__["b" /* PortfolioComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- main-menu Start -->\n<header class=\"top-area\">\n\t<div class=\"header-area\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-2\">\n\t\t\t\t\t<div class=\"logo\">\n\t\t\t\t\t\t<a href=\"/home\">\n\t\t\t\t\t\t\tedi<span>nofri</span>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div><!-- /.logo-->\n\t\t\t\t</div><!-- /.col-->\n\t\t\t\t<div class=\"col-sm-10\">\n\t\t\t\t\t<div class=\"main-menu\">\n\n\t\t\t\t\t\t<!-- Brand and toggle get grouped for better mobile display -->\n\t\t\t\t\t\t<div class=\"navbar-header\">\n\t\t\t\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-bars\"></i>\n\t\t\t\t\t\t\t</button><!-- / button-->\n\t\t\t\t\t\t</div><!-- /.navbar-header-->\n\t\t\t\t\t\t<div class=\"collapse navbar-collapse\">\n\t\t\t\t\t\t\t<app-navbar  ></app-navbar>\n\t\t\t\t\t\t</div><!-- /.navbar-collapse -->\n\t\t\t\t\t</div><!-- /.main-menu-->\n\t\t\t\t</div><!-- /.col-->\n\t\t\t</div><!-- /.row -->\n\t\t\t<div class=\"home-border\"></div><!-- /.home-border-->\n\t\t</div><!-- /.container-->\n\t</div><!-- /.header-area -->\n\n</header><!-- /.top-area-->\n<!-- main-menu End -->\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-header',
            template: __webpack_require__("./src/app/components/header/header.component.html"),
        })
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__navbar_navbar_component__ = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__navbar_navbar_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__profile_profile_component__ = __webpack_require__("./src/app/components/profile/profile.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__profile_profile_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__header_header_component__ = __webpack_require__("./src/app/components/header/header.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__header_header_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__section_section_component__ = __webpack_require__("./src/app/components/section/section.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__section_section_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__single_pack_item_single_pack_item_component__ = __webpack_require__("./src/app/components/single-pack-item/single-pack-item.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_4__single_pack_item_single_pack_item_component__["a"]; });







/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav navbar-nav navbar-right\">\n    <li class=\"smooth-menu\"><a href=\"#home\">home</a></li>\n    <li class=\"smooth-menu\" [class.hidden]='isdetail'><a href=\"#profile\">Profile</a></li>\n    <li class=\"smooth-menu\" [class.hidden]='isdetail'><a href=\"#portfolio\">Portfolio </a></li>\n    <li class=\"smooth-menu\" [class.hidden]='isdetail'><a href=\"#toolsnframeworks\">Tools & Frameworks</a></li>\n</ul>\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(route) {
        this.route = route;
        this.isdetail = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        if (this.route.snapshot.url[0].path != 'home') {
            this.isdetail = true;
        }
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"travel-box\">\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-12\">\n\t\t\t\t<div class=\"single-travel-boxes\">\n\t\t\t\t\t<div id=\"desc-tabs\" class=\"desc-tabs\">\n\n\n\t\t\t\t\t\t<!-- Tab panes -->\n\t\t\t\t\t\t<div class=\"tab-content\">\n                               <div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"hotels\">\n\t\t\t\t\t\t\t\t<div class=\"tab-para\">\n\n\t\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-12\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"single-tab-select-box\">\n\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"LI-profile-badge\"  data-version=\"v1\" data-size=\"large\" data-locale=\"in_ID\" data-type=\"vertical\" data-theme=\"light\" data-vanity=\"edinofri\"><a class=\"LI-simple-link\" href='https://id.linkedin.com/in/edinofri?trk=profile-badge'>Edinofri Karizal Caniago</a></div>\n\t\t\t\t\t\t\t\t\t\t\t</div><!--/.single-tab-select-box-->\n\t\t\t\t\t\t\t\t\t\t</div><!--/.col-->\n                                        <div class=\"col-lg-8 col-md-8 col-sm-12\">\n                                            <div class=\"about-content single-tab-select-box\">\n                                                <h2>Hei, I'm Edinofri</h2>\n                                                <p>Specialize in Android Developer and also can develop back-end for Android application base on PHP.</p>\n                                                <p>Learning something with deeply base on basic concept and open mind with out world about new style for programming.</p>\n                                                <hr>\n                                                <p>Check my latest activities on <a href=\"http:\\\\gitlab.com/edinofri\" target=\"_blank\"><button class=\"btn btn-default\"><i class=\"fab fa-gitlab\"></i> Gitlab</button></a></p>\n                                            </div>\n                                        </div>\n\n\t\t\t\t\t\t\t\t\t</div><!--/.row-->\n\n\n\t\t\t\t\t\t\t\t</div><!--/.tab-para-->\n\n\t\t\t\t\t\t\t</div><!--/.tabpannel-->\n\n\t\t\t\t\t\t</div><!--/.tab content-->\n\t\t\t\t\t</div><!--/.desc-tabs-->\n\t\t\t\t</div><!--/.single-travel-box-->\n\t\t\t</div><!--/.col-->\n\t\t</div><!--/.row-->\n\t</div><!--/.container-->\n\n</section>\n"

/***/ }),

/***/ "./src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
    }
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-profile',
            template: __webpack_require__("./src/app/components/profile/profile.component.html"),
        })
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/section/section.component.html":
/***/ (function(module, exports) {

module.exports = "<section id=\"{{idname}}\" class=\"{{classname}}\">\n\t<div class=\"container\">\n        <ng-content></ng-content>\n\t</div><!--/.container-->\n</section>\n"

/***/ }),

/***/ "./src/app/components/section/section.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SectionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SectionComponent = /** @class */ (function () {
    function SectionComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SectionComponent.prototype, "classname", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SectionComponent.prototype, "idname", void 0);
    SectionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-section',
            template: __webpack_require__("./src/app/components/section/section.component.html"),
        })
    ], SectionComponent);
    return SectionComponent;
}());



/***/ }),

/***/ "./src/app/components/single-pack-item/single-pack-item.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-4 col-sm-6\">\n\n   <div class=\"single-package-item\">\n\t\t<div class=\"pdf-thumb-box\">\n\t\t\t<a href=\"{{parent}}{{path}}\">\n\t\t\t\t<div class=\"pdf-thumb-box-overlay\">\n\t\t\t\t\t<span class=\"fa-stack fa-lg\">\n\t\t\t\t\t\t<i class=\"fa fa-eye fa-stack-1x pdf-thumb-eye\"></i>\n\t\t\t\t\t</span>\n\t\t\t\t</div>\n                <img class=\"app-logo\" src=\"{{image}}\" alt=\"\" >\n\t\t\t\t<img src=\"assets/images/packages/p1.jpg\" alt=\"package-place\">\n\n\t\t\t</a>\n\t\t</div>\n\n\n\t</div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/single-pack-item/single-pack-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SinglePackItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SinglePackItemComponent = /** @class */ (function () {
    function SinglePackItemComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SinglePackItemComponent.prototype, "name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SinglePackItemComponent.prototype, "path", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SinglePackItemComponent.prototype, "parent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], SinglePackItemComponent.prototype, "image", void 0);
    SinglePackItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-single-pack-item',
            template: __webpack_require__("./src/app/components/single-pack-item/single-pack-item.component.html"),
        })
    ], SinglePackItemComponent);
    return SinglePackItemComponent;
}());



/***/ }),

/***/ "./src/app/pages/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_index_component__ = __webpack_require__("./src/app/pages/index/index.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__index_index_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__portfolio_portfolio_component__ = __webpack_require__("./src/app/pages/portfolio/portfolio.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__portfolio_portfolio_component__["a"]; });




/***/ }),

/***/ "./src/app/pages/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n\n<app-section idname=\"home\" classname=\"about-us\">\n\t<div class=\"container\" id=\"profile\">\n\t\t<div class=\"about-us-content\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t<div class=\"single-about-us\">\n\t\t\t\t\t\t<div class=\"about-us-txt\">\n\t\t\t\t\t\t\t<h2 class=\"animated fadeInUp\" style=\"opacity: 0;\">\n\t\t\t\t\t\t\t\tHi, I'm Edinofri Karizal Caniago\n\t\t\t\t\t\t\t</h2>\n\n\t\t\t\t\t\t</div><!--/.about-us-txt-->\n\t\t\t\t\t</div><!--/.single-about-us-->\n\t\t\t\t</div><!--/.col-->\n\t\t\t\t<div class=\"col-sm-0\">\n\t\t\t\t\t<div class=\"single-about-us\">\n\n\t\t\t\t\t</div><!--/.single-about-us-->\n\t\t\t\t</div><!--/.col-->\n\t\t\t</div><!--/.row-->\n\t\t</div><!--/.about-us-content-->\n\t</div><!--/.container-->\n</app-section>\n<app-profile></app-profile>\n<app-section idname=\"portfolio\" classname=\"service\">\n\t<div class=\"gallery-details\">\n\t\t<div class=\"gallary-header text-center\">\n\t\t\t<h2>\n\t\t\t\tA few recent works\n\t\t\t</h2>\n\t\t\t<p>\n\t\t\t\tSome of the apps I'm contributing to\n\t\t\t</p>\n\t\t</div><!--/.gallery-header-->\n\t\t<div class=\"packages-content\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<app-single-pack-item name=\"Solvin\" parent=\"/portfolio\" path=\"/id.solvin.app\" image=\"assets/logo/solvin.png\">\n\t\t\t\t</app-single-pack-item>\n\n\t\t\t\t<app-single-pack-item name=\"InGo\" parent=\"/portfolio\" path=\"/io.nexia.ingo\" image=\"assets/logo/ingo.png\">\n\t\t\t\t</app-single-pack-item>\n\n\t\t\t\t<app-single-pack-item name=\"SimpliDots Live Tracking\" parent=\"/portfolio\" path=\"/com.simplidots.services\" image=\"assets/logo/simplidots.png\">\n\t\t\t\t</app-single-pack-item>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</app-section>\n\n<app-section classname=\"testemonial\" idname=\"toolsnframeworks\">\n\t\t<div class=\"gallary-header text-center\">\n\t\t\t<h2>\n\t\t\t\tTools & Frameworks\n\t\t\t</h2>\n\t\t</div><!--/.gallery-header-->\n\t\t<div class=\"packages-content\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div *ngFor=\"let t of toolsframeworks\" class=\"col-md-2 text-center add-margin-bot\">\n\t\t\t\t\t<img src=\"{{t.image}}\" width=\"100\" height=\"100\" >\n\t\t\t\t\t<h4 class=\"add-margin-top\">{{t.name}}</h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n</app-section>\n\n<footer class=\"footer-copyright\">\n\t<div class=\"foot-icons \">\n\t\t<ul class=\"footer-social-links list-inline list-unstyled\">\n\t        <li><a href=\"http://fb.me/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-1\"><i class=\"fab fa-facebook\"></i></a></li>\n\t        <li><a href=\"http://twitter.com/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-2\"><i class=\"fab fa-twitter\"></i></a></li>\n\t\t\t<li><a href=\"http://instagram.com/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-3\"><i class=\"fab fa-instagram\"></i></a></li>\n\t\t\t<li><a href=\"http://gitlab.com/edinofri\" target=\"_blank\" class=\"foot-icon-bg-4\"><i class=\"fab fa-gitlab\"></i></a></li>\n\t        <li><a href=\"http://linkedin.com/in/edinofri\" target=\"_blank\" class=\"foot-icon-bg-2\"><i class=\"fab fa-linkedin\"></i></a></li>\n\t\t</ul>\n\t\t<p>© 2018 Theme by <a href=\"https://www.themesine.com\">ThemeSINE</a>. All Right Reserved</p>\n\n\t</div>\n\t<div id=\"scroll-Top\">\n\t\t<i class=\"fa fa-angle-double-up return-to-top\" id=\"scroll-top\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Back to Top\" aria-hidden=\"true\" style=\"display: inline;\"></i>\n\t</div>\n</footer>\n"

/***/ }),

/***/ "./src/app/pages/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IndexComponent = /** @class */ (function () {
    function IndexComponent() {
        this.toolsframeworks = [];
        this.toolsframeworks = [
            { name: "Ubuntu", image: "assets/logo/ubuntu.png" },
            { name: "Laravel", image: "assets/logo/laravel.png" },
            { name: "Lumen", image: "assets/logo/lumen.png" },
            { name: "Android Studio", image: "assets/logo/androidstudio.png" },
            { name: "Atom", image: "assets/logo/atom.png" },
            { name: "Angular", image: "assets/logo/angular.png" },
            { name: "Bootstrap", image: "assets/logo/bootstrap.svg" },
            { name: "Gitlab", image: "assets/logo/gitlab.png" },
            { name: "Draw.IO", image: "assets/logo/drawio.png" },
            { name: "MySQL", image: "assets/logo/mysql.jpg" },
            { name: "Firebase", image: "assets/logo/firebase.png" },
        ];
    }
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-index',
            template: __webpack_require__("./src/app/pages/index/index.component.html"),
        }),
        __metadata("design:paramtypes", [])
    ], IndexComponent);
    return IndexComponent;
}());



/***/ }),

/***/ "./src/app/pages/portfolio/portfolio.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n\n<app-section idname=\"home\" classname=\"about-us\">\n\t<div class=\"container\" id=\"profile\">\n\t\t<div class=\"about-us-content\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t<div class=\"single-about-us\">\n\t\t\t\t\t\t<div class=\"about-us-txt  animated fadeInUp\"  style=\"background-color:rgba(255,255,255,0.9);max-width:900px;margin:0 auto\">\n                            <div class=\"row\">\n                                <div class=\"col-md-4 text-center\" style=\"padding:20px 20px\">\n                                        <img class=\"animated fadeInUp\" src=\"{{item.image}}\" alt=\"\" width=\"150px\" height=\"150px\">\n                                        <br>\n                                        <a href=\"{{item.url}}\" target=\"_blank\"><h3>{{item.name}}</h3></a>\n                                </div>\n                                <div class=\"col-md-8\"  style=\"padding:20px\">\n                                    <h4 class=\"add-margin-bot\">Description</h4>\n                                    <p class='add-margin-bot'>{{item.desc}}</p>\n                                    <hr>\n                                    <h4 class=\"add-margin-bot\">Role</h4>\n                                    <p>Sebagai {{item.role}}</p>\n                                    <hr>\n                                    <h4 class=\"add-margin-bot\">Tools</h4>\n                                    <img *ngFor=\"let t of item.tools\"src=\"{{t.image}}\" width=\"50px\" height=\"50px\" title=\"{{t.name}}\" alt=\"\">\n                                    <hr>\n                                    <a *ngIf=\"item.playstore\" href=\"{{item.playstore}}\" target=\"_blank\"><img src=\"assets/logo/playstore.png\" alt=\"\"> </a>\n                                </div>\n                            </div>\n\t\t\t\t\t\t</div><!--/.about-us-txt-->\n\t\t\t\t\t</div><!--/.single-about-us-->\n\t\t\t\t</div><!--/.col-->\n\t\t\t\t<div class=\"col-sm-0\">\n\t\t\t\t\t<div class=\"single-about-us\">\n\n\t\t\t\t\t</div><!--/.single-about-us-->\n\t\t\t\t</div><!--/.col-->\n\t\t\t</div><!--/.row-->\n\t\t</div><!--/.about-us-content-->\n\t</div><!--/.container-->\n</app-section>\n<footer class=\"footer-copyright\">\n\t<div class=\"foot-icons \">\n\t\t<ul class=\"footer-social-links list-inline list-unstyled\">\n\t        <li><a href=\"http://fb.me/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-1\"><i class=\"fab fa-facebook\"></i></a></li>\n\t        <li><a href=\"http://twitter.com/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-2\"><i class=\"fab fa-twitter\"></i></a></li>\n\t\t\t<li><a href=\"http://instagram.com/tukangbasic\" target=\"_blank\" class=\"foot-icon-bg-3\"><i class=\"fab fa-instagram\"></i></a></li>\n\t\t\t<li><a href=\"http://gitlab.com/edinofri\" target=\"_blank\" class=\"foot-icon-bg-4\"><i class=\"fab fa-gitlab\"></i></a></li>\n\t        <li><a href=\"http://linkedin.com/in/edinofri\" target=\"_blank\" class=\"foot-icon-bg-2\"><i class=\"fab fa-linkedin\"></i></a></li>\n\t\t</ul>\n\t\t<p>© 2018 Theme by <a href=\"https://www.themesine.com\">ThemeSINE</a>. All Right Reserved</p>\n\n\t</div>\n\t<div id=\"scroll-Top\">\n\t\t<i class=\"fa fa-angle-double-up return-to-top\" id=\"scroll-top\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Back to Top\" aria-hidden=\"true\" style=\"display: inline;\"></i>\n\t</div>\n</footer>\n"

/***/ }),

/***/ "./src/app/pages/portfolio/portfolio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortfolioComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PortfolioComponent = /** @class */ (function () {
    function PortfolioComponent(route) {
        this.route = route;
        this.items = [];
        this.item = { name: "edi" };
        this.items = [
            {
                name: "Solvin",
                slug: 'id.solvin.app',
                url: "http://solvin.id",
                image: "assets/logo/solvin.png",
                desc: "Sebuah aplikasi dengan layanan tanya-jawab dari pelajar kepada mentor-mentor yang terkualifikasi. Solvin menyediakan solusi terbaik, tercepat, dan terlengkap untuk setiap pertanyaan yang diajukan.",
                playstore: "https://play.google.com/store/apps/details?id=id.solvin.app",
                role: "Android Programmer & Co-Founder",
                tools: [
                    { image: "assets/logo/androidstudio.png", name: "android studio" },
                    { image: "assets/logo/ubuntu.png", name: "ubuntu" },
                    { image: "assets/logo/laravel.png", name: "laravel" },
                    { image: "assets/logo/firebase.png", name: "Firebase" },
                ]
            },
            {
                name: "InGo",
                slug: 'io.nexia.ingo',
                url: "http://ingo.io/",
                image: "assets/logo/ingo.png",
                desc: "Have you ever used a newspaper? According to Wikipedia, a newspaper is a serial publications containing news, other informative articles, and advertising. If a newspaper is the body, then data should be the mind. inGO restructure the mind and the body for it to be accessible to mobile user.",
                playstore: "https://play.google.com/store/apps/details?id=io.nexia.ingo",
                role: "Android Programmer",
                tools: [
                    { image: "assets/logo/androidstudio.png", name: "android studio" },
                    { image: "assets/logo/firebase.png", name: "Firebase" },
                ]
            },
            {
                name: "SimpliDots Live Tracking",
                slug: 'com.simplidots.services',
                url: "http://simplidots.com/",
                image: "assets/logo/simplidots.png",
                desc: "Sebuah aplikasi yang hanya berupa background service untuk melacak keberadaan Salesman Secara Realtime.",
                playstore: null,
                role: "Android Programmer",
                tools: [
                    { image: "assets/logo/androidstudio.png", name: "android studio" },
                    { image: "assets/logo/xamarin.png", name: "Xamarin" },
                    { image: "assets/logo/firebase.png", name: "Firebase" },
                ]
            },
        ];
    }
    PortfolioComponent.prototype.ngOnInit = function () {
        // 'bank' is the name of the route parameter
        var routeparam = this.route.snapshot.params['name'];
        this.item = this.items.filter(function (x) { return x.slug === routeparam; })[0];
        console.log(this.item);
    };
    PortfolioComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-portfolio',
            template: __webpack_require__("./src/app/pages/portfolio/portfolio.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], PortfolioComponent);
    return PortfolioComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map