import {Component,Input} from '@angular/core';

@Component({
    selector: 'app-single-pack-item',
    templateUrl: './single-pack-item.component.html',
})

export class SinglePackItemComponent{
    @Input() name:string;
    @Input() path:string;
}
