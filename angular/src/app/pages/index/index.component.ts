import {Component} from '@angular/core';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
})

export class IndexComponent {
    toolsframeworks:any[] = [];
    constructor(){
        this.toolsframeworks = [
            {name:"Ubuntu",image:"assets/logo/ubuntu.png"},
            {name:"Laravel",image:"assets/logo/laravel.png"},
            {name:"Lumen",image:"assets/logo/lumen.png"},
            {name:"Android Studio",image:"assets/logo/androidstudio.png"},
            {name:"Atom",image:"assets/logo/atom.png"},
            {name:"Angular",image:"assets/logo/angular.png"},
            {name:"Bootstrap",image:"assets/logo/bootstrap.svg"},
            {name:"Gitlab",image:"assets/logo/gitlab.png"},
            {name:"Draw.IO",image:"assets/logo/drawio.png"},
            {name:"MySQL",image:"assets/logo/mysql.jpg"},

        ];
    }
}
